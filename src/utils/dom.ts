/**
 * 向上查找父级节点
 * @param child
 * @param parentClassName
 */
export const getTopLevelParent = (child: HTMLDivElement, parentClassName: string) => {
  if (child.className.includes(parentClassName)) {
    return child;
  } else {
    let parent = child.parentNode as HTMLDivElement;
    let isFind = true;
    while (isFind) {
      if (parent && parent.className.includes(parentClassName)) {
        isFind = false;
      } else {
        parent = parent.parentNode as HTMLDivElement;
      }
    }
    return parent;
  }
};
