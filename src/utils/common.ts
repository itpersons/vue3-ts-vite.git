class DeepClone {
  static intrinsicTypeNames =
    'Boolean,String,Date,RegExp,Blob,File,FileList,FileSystemFileHandle,FileSystemDirectoryHandle,ArrayBuffer,DataView,Uint8ClampedArray,ImageBitmap,ImageData,Map,Set,CryptoKey'
      .split(',')
      .concat(
        [].concat.apply(
          [8, 16, 32, 64].map(function (num) {
            return ['Int', 'Uint', 'Float'].map(function (t) {
              return t + num + 'Array';
            });
          })
        )
      )
      .filter((t) => {
        return window[t];
      });
  static intrinsicTypes = this.intrinsicTypeNames.map((t) => {
    return window[t];
  });
  static innerDeepClone(object: any) {
    if (!object || typeof object !== 'object') return object;
    let rv = this.circularRefs && this.circularRefs.get(object);
    if (rv) return rv;
    if (Array.isArray(object)) {
      rv = [];
      this.circularRefs && this.circularRefs.set(object, rv);
      for (let i = 0, l = object.length; i < l; ++i) {
        rv.push(this.innerDeepClone(object[i]));
      }
    } else if (this.intrinsicTypes.indexOf(object.constructor) >= 0) {
      rv = object;
    } else {
      const proto = Object.getPrototypeOf(object);
      rv = proto === Object.prototype ? {} : Object.create(proto);
      this.circularRefs && this.circularRefs.set(object, rv);
      for (const prop in object) {
        if ({}.hasOwnProperty.call(object, prop)) {
          rv[prop] = this.innerDeepClone(object[prop]);
        }
      }
    }
    return rv;
  }
  static circularRefs: any = null;
  static deepClone(object: any) {
    this.circularRefs = typeof WeakMap !== 'undefined' && new WeakMap();
    const rv = this.innerDeepClone(object);
    this.circularRefs = null;
    return rv;
  }
}

/**
 * 深度克隆对象
 * @param object 参数对象
 * @returns 返回克隆后的对象
 */
export const deepClone = <T>(object: T): T => {
  return DeepClone.deepClone(object) as T;
};

/**
 * 获取对象中有值的属性
 * @param obj 需要处理的对象
 * @param depth 是否深度处理
 * @returns 返回有值的属性对象
 */
export const getPropertyValueObj = <T>(obj: T, depth: boolean = false): Record<string, any> => {
  const keys = Object.keys(obj as Object);
  const result: Record<string, any> = {};
  if (keys.length) {
    for (let index = 0; index < keys.length; index++) {
      const key = keys[index];
      if (obj[key] !== '' && obj[key] !== null && obj[key] !== undefined) {
        if (depth && obj[key] instanceof Object) {
          result[key] = getPropertyValueObj(obj[key], depth);
          continue;
        }
        result[key] = obj[key];
      }
    }
  }
  return result;
};

/**
 * 将数字转成中文大写
 * @param {*} num
 */
export const toChinesNum = (num: number) => {
  const changeNum = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
  const unit = ['', '十', '百', '千', '万'];
  // num = parseInt(num);
  if (num > 9 && num < 20) {
    if (num === 10) {
      return '十';
    } else {
      const index = num.toString().split('')[1];
      return '十' + changeNum[index];
    }
  }
  const getWan = (temp: number | string) => {
    const strArr: any[] = temp.toString().split('').reverse();
    let newNum = '';
    for (let i = 0; i < strArr.length; i++) {
      newNum =
        (i == 0 && strArr[i] == 0
          ? ''
          : i > 0 && strArr[i] == 0 && strArr[i - 1] == 0
            ? ''
            : changeNum[strArr[i]] + (strArr[i] == 0 ? unit[0] : unit[i])) + newNum;
    }
    return newNum;
  };
  const overWan = Math.floor(num / 10000);
  let noWan: string | number = num % 10000;
  if (noWan.toString().length < 4) noWan = '0' + noWan;
  return overWan ? getWan(overWan) + '万' + getWan(noWan) : getWan(num);
};

/**
 * 生成uuid
 * @param len 长度
 * @param radix 基数
 * @returns
 */
export const uuid2 = (len: number, radix: number) => {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
  const uuid = [];
  let i: number;
  radix = radix || chars.length;
  if (len) {
    for (i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * radix)];
  } else {
    let r;
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
    uuid[14] = '4';
    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | (Math.random() * 16);
        uuid[i] = chars[i == 19 ? (r & 0x3) | 0x8 : r];
      }
    }
  }
  return uuid.join('');
};

/**
 * 判断字符中是否包含表情
 * @param str 字符内容
 * @returns
 */
export const isEmoji = (str: string) => {
  // utf8mb4 正则
  const utf8mb4Reg = /[^\u0000-\uFFFF]/g; // eslint-disable-line
  // emoji 表情正则
  const emojiReg =
    /[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF][\u200D|\uFE0F]|[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF]|[0-9|*|#]\uFE0F\u20E3|[0-9|#]\u20E3|[\u203C-\u3299]\uFE0F\u200D|[\u203C-\u3299]\uFE0F|[\u2122-\u2B55]|\u303D|[\A9|\AE]\u3030|\uA9|\uAE|\u3030/gi; // eslint-disable-line
  // 包含 utf8mb4 的 4 字节
  const isUTF8MB4 = utf8mb4Reg.test(str);
  // 包含 emoji 表情
  const isEmoji = emojiReg.test(str);

  return isUTF8MB4 || isEmoji;
};
