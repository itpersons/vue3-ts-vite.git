import { deepClone, getPropertyValueObj, toChinesNum, isEmoji } from '../common';

// 测试用例
describe('utils common', () => {
  let obj = {};
  beforeEach(() => {
    // 测试前的准备
    obj = {
      name: 'test',
      age: 18,
      obj: {
        name: 'test',
        age: 90
      }
    };
  });

  it('isEmoji Function', () => {
    const emoji = '发的撒😊放大机看';
    const nonEmoji = '到付即可';
    expect(isEmoji(emoji)).toBe(true);
    expect(isEmoji(nonEmoji)).toBe(false);
  });

  it('toChinesNum Function', () => {
    const num = toChinesNum(10);
    expect(num).toBe('十');
    const num1 = toChinesNum(11);
    expect(num1).toBe('十一');
    const num2 = toChinesNum(100);
    expect(num2).toBe('一百');
    const num5 = toChinesNum(101);
    expect(num5).toBe('一百零一');
    const num3 = toChinesNum(1000);
    expect(num3).toBe('一千');
    const num4 = toChinesNum(10000);
    expect(num4).toBe('一万');
  });

  it('deepClone Function', () => {
    expect(obj).toBe(obj);
    const cloneObj = deepClone(obj);
    expect(cloneObj).not.toBe(obj);
    expect(cloneObj).toStrictEqual(obj);
  });

  it('getPropertyValueObj Function', () => {
    const obj = {
      name: 'test',
      age: 18,
      q: null,
      b: '',
      c: undefined,
      d: false,
      e: 0,
      child: { name: 'child', age: 90, q: null, b: '', c: undefined, d: false, e: 0 }
    };
    const yq = {
      name: 'test',
      age: 18,
      d: false,
      e: 0,
      child: { name: 'child', age: 90, d: false, e: 0 }
    };
    let valueObj = getPropertyValueObj(obj, true);
    // console.log(valueObj)
    expect(valueObj).toStrictEqual(yq);
    valueObj = getPropertyValueObj(obj, false);
    yq.child = { name: 'child', age: 90, q: null, b: '', c: undefined, d: false, e: 0 } as any;
    // console.log(valueObj)
    expect(valueObj).toStrictEqual(yq);
  });
});
